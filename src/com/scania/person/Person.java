package com.scania.person;

import java.util.Scanner;

public class Person

{
    private String firstName;
    private  String lastName;
    private  String species;
    private  int age;
    private  int height;
    private  String nationality;
    private  String language;
    private  String hometown;
    private  String profession;
    private  String hasaThingFor;
    private  String identifiesAs;
    private  String pets;
    private boolean isaCrook;

    //constructor:

    public Person(String firstName, String lastName, String species, int age, int height, String nationality,
                  String language, String hometown, String profession, String hasaThingFor, String  identifiesAs,
                  String pets, boolean isaCrook) {


    this.firstName = firstName;
    this.lastName = lastName;
    this.species = species;
    this.age = age;
    this.height = height;
    this.nationality = nationality;
    this.language = language;
    this.hometown = hometown;
    this.profession = profession;
    this.hasaThingFor = hasaThingFor;
    this.identifiesAs = identifiesAs;
    this.pets = pets;
    this.isaCrook = isaCrook;


    }

    public String presentYourself() {

        return

                "Hi, thats me, " + firstName + " " + lastName + ". " + "I am" + " " + age + " years old " + "and" + " " + height + " " + "cm tall" + "."
                + " I am " + nationality + " and the language I speak is " + language + "." + " I am a " + species + " but I prefer to identify myself as " +
                identifiesAs + "." + " Profession? Nothing special just " + profession + ". " + "Let me tell you a thing though, seriously, I really have a thing for "
                        + hasaThingFor + ", I just cannot deny it! " + "You probably wonder if I have a pet don't you? " + pets + "!";


    }

    final double inch = 0.394;

    public String getHeightInches(){

        return

                firstName + "s" + " height is " + height * inch + " inches.";

    }

    public String iKnowYou(Person iKnowUperson) {

        if(iKnowUperson.hometown.equals(hometown)){

            return
                    "I know you!";
        }

        else
            return presentYourself();

    }
}